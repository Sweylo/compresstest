/*

Out - class for convenient output to the console

Copyright (c) 2017, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
fee is hereby granted, provided that the above copyright notice and this permission notice appear 
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.

*/

package compresstest;

import java.io.PrintStream;

/**
 * @author sweylo
 */
public class Out {
	
	private static final PrintStream OUT = System.out;
	private static final PrintStream ERR = System.err;
	
	public static final int STD_LINE_WIDTH = 80;

	public static void stdOut(String output) {
		OUT.print(output);
	}
	
	public static void stdOut(String output, boolean makeNewLine) {
		
		OUT.print(output);
		
		if (makeNewLine) {
			newLine();
		}
		
	}
	
	public static void stdErr(String output) {
		ERR.print(output);
	}
	
	public static void stdErr(String output, boolean makeNewLine) {
		
		ERR.print(output);
		
		if (makeNewLine) {
			newLine();
		}
		
	}
	
	public static void stdOutHeading(String text, int width) {
		
		if (text.length() < width - 4) {
			
			if (text.equals("")) {
				OUT.print("+--");
			} else {
				OUT.print("+ " + text + " ");
			}
			
			for (int i = 0; i < width - text.length(); i++) {
				OUT.print("-");
			}
			
			newLine();
			
		}
		
	}
	
	public static void newLine() {
		OUT.println();
	}
	
}

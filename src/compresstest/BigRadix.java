/*

BigRadix class

Copyright (c) 2017, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
fee is hereby granted, provided that the above copyright notice and this permission notice appear 
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.

*/

package compresstest;

import java.util.ArrayList;

/**
 * class to handle large integers of any radix and to convert to any radix
 * 
 * @author sweylo
 */
public class BigRadix extends ArrayList<Byte> {
	
	private byte radix;
	
    public BigRadix() {}
    
	public BigRadix(byte radix) {
		this.radix = radix;
	}
	
	public BigRadix(ArrayList<Byte> decArray, byte radix) {
        
		super(decArray);
		this.radix = radix;
        
		for (Byte digit : this) {
            
			if (isTooBig(digit)) {
                
				System.err.printf("Unable to convert: digit [%d] is too big for radix [%d].\n", 
					digit, radix);
                
			}
            
		}
        
	}
	
	private boolean isTooBig(byte digit) {
		return digit >= radix;
	}
	
	public boolean quickAdd(Byte digit) {
		return super.add(digit);
	}
	
	public void setRadix(byte radix) {
		
		if (radix > this.radix) {
			
		} else if (radix < this.radix) {
			
		} else {
			System.err.println("Radix is the same.");
		}
		
		this.radix = radix;
		
	}
	
	@Override
	public boolean add(Byte digit) {
		
		if (isTooBig(digit)) {
			System.err.printf("Unable to add: digit [%d] is too big for radix [%d].\n", 
				digit, radix);
		}
		
		return super.add(digit);
		
	}
	
	/*@Override
    public String toString() {
		
    }*/
    
}

/*

Run - class to handle the runs of binary data

Copyright (c) 2017, Logan Matthew Nichols

Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
fee is hereby granted, provided that the above copyright notice and this permission notice appear 
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF 
THIS SOFTWARE.

*/

package compresstest;

import java.util.ArrayList;

/**
 * @author sweylo
 */
public class DataList extends ArrayList<Integer> {
	
	public int getSum() {
		return getSum(0);
	}
    
    public int getSum(int floor) {
        
        int sum = 0;
		
		for (int run : this) {
            if (run >= floor) {
                sum += run;
            }
		}
		
		return sum;
        
    }
	
	public int getMax() {
		
		int maxRun = 0;
		
		for (int run : this) {
			if (run > maxRun) {
				maxRun = run;
			}
		}
		
		return maxRun;
		
	}
	
	public int getMean() {
		return getMean(0);
	}
    
    public int getMean(int floor) {
        
        int mean = 0;
		
		if (size() > 0) {
			mean = getSum(floor) / size(floor);
		} else {
			Out.stdErr("Array has no data, will result in division by 0.", true);
		}
		
		return mean;
        
    }
    
    public int size(int floor) {
        
        int size = 0;
        
        for (int run : this) {
            if (run >= floor) {
                size++;
            }
        }
        
        return size;
        
    }
	
}

/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package compresstest;

import static compresstest.Out.*;
import patterncompress.*;

/**
 * @author sweylo
 */
public class CompressTest {
    
    // constants
    public static final String TEST_INPUT_FILE_PATH = "test.txt";
    public static final String TEST_INPUT_LIPSUM_PATH = "lipsum.txt";
    public static final String TEST_INPUT_LOSSLESS_IMAGE_PATH = "moon.png";
    public static final String TEST_INPUT_RAW_IMAGE_PATH = "IMG_2246.CR2";
    public static final String TEST_INPUT_SMALL_IMAGE_PATH = "small_image.jpg";
    public static final String TEST_OUTPUT_FILE_PATH = "output.mith";
    public static final int FILE_CHAR_BIT_LENGTH = 8;
    
    public static void main(String[] args) {
        
        stdOutHeading("CompressTest.main()", STD_LINE_WIDTH);
        
        // read in the test file to a string containing the binary value of the file
//        Binary binFile = Binary.readFileToBinary(TEST_INPUT_LIPSUM_PATH, FILE_CHAR_BIT_LENGTH);
//        Binary binFile = Binary.readFileToBinary(TEST_INPUT_SMALL_IMAGE_PATH, FILE_CHAR_BIT_LENGTH);
        Binary binFile = Binary.readFileToBinary(TEST_INPUT_LOSSLESS_IMAGE_PATH, FILE_CHAR_BIT_LENGTH);
        
        DataList runRecord = binFile.getRuns();
        
//        System.out.println(runRecord);
        System.out.println("run avg: " + runRecord.getMean(6));
        System.out.println("run max: " + runRecord.getMax());
        
        Timer pcTimer = new Timer();
        pcTimer.start();
        
        PatternData pd = new PatternData(binFile, 4, 100, 12);
        Binary compressedData = pd.compress();
        
        pcTimer.stop();
        
        String timeUnit = "s";
        System.out.printf("Compression time: %6.3f %s\n", pcTimer.getElapsed(timeUnit), timeUnit);
        
//        System.out.println("File contents: " + binFile);
//        System.out.printf("Original size: %d bits (%d bytes) \n", 
//           binFile.size(), binFile.size() / 8);
        
        System.out.printf("Compressed size: %d bits (%f bytes) \n", 
            compressedData.size(), (double) compressedData.size() / 8);
//        System.out.println(pd);
        System.out.println("Compressed contents: " + compressedData);
        
        // output compression ratio
        System.out.printf("Compression ratio: %6.3f:1 (%6.3f%%)\n", 
            (double) binFile.size() / compressedData.size(), 
            (double) compressedData.size() / binFile.size());
        
    }
    
}

/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package compresstest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

/**
 * @author sweylo
 */
public class Binary extends ArrayList<Boolean> {
	
	public static final boolean ZERO = false;
	public static final boolean ONE = true;
    
    public Binary() {}
    
    public Binary(Boolean[] data) {
        super(Arrays.asList(data));
    }
	
    public static Binary readFileToBinary(String filePath, int fileCharBitLength) {
        
        System.out.printf("Binary.readFileToBinary('%s')\n", filePath);
        
        Timer fileReadTimer = new Timer();
        fileReadTimer.start();
        
        File file = new File(filePath);
        Binary bin = new Binary();
        
        if (file.canRead()) {
            
            try {
                
                FileInputStream input = new FileInputStream(file);
                char current = (char)input.read();
                
                while(current != Character.MAX_VALUE) {
                    bin.addAll(decToBin(current, fileCharBitLength, ZERO));
                    current = (char)input.read();
                }
                
                //System.out.println(bin.toString());
                //System.out.printf("Original size: %d bytes\n", origSize);
                
            } catch (FileNotFoundException e) {
                System.err.println("Unable to find file.");
                System.exit(2);
            } catch (IOException e) {
                System.err.println("Error reading file."); 
                System.exit(3);
            }
            
        } else {
            System.err.println("Unable to read file.");
            System.exit(4);
        }
        
        fileReadTimer.stop();
		String timeUnit = "s";
        System.out.printf("file read time: %f %s\n", fileReadTimer.getElapsed(timeUnit), timeUnit);
        
        return bin;
        
    }
    
    public static void writeBinaryToFile(String filePath, Binary data) {
        
    }
    
    public DataList getRuns() {
        
        //System.out.println("RecordRuns.encode()");
        
        DataList runRecord = new DataList();
        boolean curr = get(0);
        int count = 1;

        for (int i = 1; i < size(); i++) {
            if (get(i) == curr) {
                count++;
            } else {
                runRecord.add(count);
                //System.out.println(count + " - " + curr);
                curr = get(i);
                count = 1;
            }
        }
        
        return runRecord;
            
    }
    
    public static Binary decToBin(int dec, int length, boolean padState) {
        
        //System.out.printf("decToBin(%3d) | elapsed ", dec);
        
        //Timer decToBinTimer = new Timer();
        //decToBinTimer.start();
        
        //System.out.println("CompressTest.decToBin()");
        
		// initialize a stack for building the binary number and arraylist to return
        Stack<Boolean> binStack = new Stack<>();
        Binary binArray = new Binary();
        
		// push the binary digits found to the top of the stack
        do {
            binStack.push((dec % 2) > 0);
            dec /= 2;
        } while (dec > 0);
        
		// push any additional zeroes necessary to fit the number of bits specified
        while (binStack.size() < length) {
            binStack.push(padState);
        }
        
		// add each element in the stack to the arraylist (reverses the order)
        for (int i = 0; i < length; i++) {
            binArray.add(binStack.pop());
        }
        
        //String timeUnit = "ms";
        //decToBinTimer.stop();
        //System.out.printf("(%s): %9.6f\n", timeUnit, decToBinTimer.getElapsed(timeUnit));
        
        return binArray;
        
    }
    
    public static Binary decToBin(int dec) {
        
        //System.out.printf("decToBin(%3d) | elapsed ", dec);
        
        //Timer decToBinTimer = new Timer();
        //decToBinTimer.start();
        
        //System.out.println("CompressTest.decToBin()");
        
		// initialize a stack for building the binary number and arraylist to return
        Stack<Boolean> binStack = new Stack<>();
        Binary binArray = new Binary();
        
		// push the binary digits found to the top of the stack
        do {
            binStack.push((dec % 2) > 0);
            dec /= 2;
        } while (dec > 0);
        
		// add each element in the stack to the arraylist (reverses the order)
        for (int i = 0; i < binStack.size(); i++) {
            binArray.add(binStack.pop());
        }
        
        //String timeUnit = "ms";
        //decToBinTimer.stop();
        //System.out.printf("(%s): %9.6f\n", timeUnit, decToBinTimer.getElapsed(timeUnit));
        
        return binArray;
        
    }
    
	@Override
	public String toString() {
		
		StringBuilder binBuilder = new StringBuilder();
		
		for (Boolean bit : this) {
			binBuilder.append(bit ? 1 : 0);
		}
		
		return binBuilder.toString();
		
	}
	
}

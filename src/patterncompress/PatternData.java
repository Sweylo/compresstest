/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package patterncompress;

import compresstest.Binary;
import static compresstest.Binary.*;
import static compresstest.Out.*;
import compresstest.DataList;
import java.util.ArrayList;

/**
 *
 * @author sweylo
 */
public class PatternData extends ArrayList<PatternDatum> {
    
    private int maxRunBitCount;
    private int maxRawBitCount;
    private int minRunBitCount;
    private final Binary data;
    
    public PatternData(Binary data, int maxRunBitCount, int maxRawBitCount, int minRunBitCount) {
        
        System.out.printf("PatternData.construct("
            + "data: %d bits, maxRunBitCount: %d, maxRawBitCount: %d, minRunBitCount: %d)\n", 
            data.size(), maxRunBitCount, maxRawBitCount, minRunBitCount);
        
        this.data = data;
        this.maxRawBitCount = maxRawBitCount;
        this.minRunBitCount = minRunBitCount;
        
        if (maxRunBitCount >= decToBin(minRunBitCount).size()) {
            this.maxRunBitCount = maxRunBitCount;
        } else {
            System.err.printf("Length of run (%d) not meet min run length (%d)\n", 
                maxRunBitCount, minRunBitCount);
            System.exit(1);
        }
        
    }
    
    public Binary compress() {
        
        Binary compressed = new Binary();
        DataList dl = data.getRuns();
        boolean currentBit = data.get(0);
        
        //for (Integer length : rl) {
        for (int i = 0; i < dl.size() - 1; i++) {
            
//            stdOutHeading(String.format(
//                "PatternData.compress() | i: %d, dl.get(%d): %d", i, i, dl.get(i)
//            ), STD_LINE_WIDTH);
            
//            System.out.println();
            
            // run
            if (dl.get(i) >= minRunBitCount) {
            
                //System.out.println("g:" + rl.get(i));
                
                this.add(currentBit, dl.get(i));
                
            // raw
            } else {
                
                Binary thisRaw = new Binary();
                int currentBitCount = 0;
                
                while (currentBitCount + dl.get(i) <= maxRawBitCount && i < dl.size() - 1) {
                    
//                    System.out.printf("\ti: %d | currentBit: %d | length: %d\n", 
//                        i, currentBit ? 1 : 0, dl.get(i));
                    
                    for (int j = 0; j < dl.get(i); j++) {
                        thisRaw.add(currentBit);
                    }
                    
                    currentBitCount += dl.get(i);
                    currentBit = !currentBit;
                    
                    i++;
                    
                }
                
                i--;
                
//                System.out.println("\tthisRaw: " + thisRaw + "\n");
                
                this.add(thisRaw);
                
            }
            
            currentBit = !currentBit;
            
        }
        
        //System.out.println(this);
        
        for (PatternDatum pd : this) {
            compressed.addAll(pd.toBinary());
        }
        
        return compressed;
        
    }
    
    public static Binary decompress(Binary bin) {
        
        Binary original = new Binary();
        
        
        
        return original;
        
    }

    public void add(boolean bit, int count) {
        super.add(new PatternRun(bit, count, maxRunBitCount));
    }
    
    public void add(Binary raw) {
        super.add(new PatternRaw(raw, maxRawBitCount));
    }
    
    @Override
	public String toString() {
		
		StringBuilder binBuilder = new StringBuilder();
		
		for (PatternDatum pd : this) {
			binBuilder.append(pd.toString());
            binBuilder.append("\n");
		}
		
		return binBuilder.toString();
		
	}

    /**
     * @param maxRunBitCount the maxRunBitCount to set
     */
    public void setMaxRunBitCount(int maxRunBitCount) {
        this.maxRunBitCount = maxRunBitCount;
    }

    /**
     * @param maxRawBitCount the maxRawBitCount to set
     */
    public void setMaxRawBitCount(int maxRawBitCount) {
        this.maxRawBitCount = maxRawBitCount;
    }
    
}

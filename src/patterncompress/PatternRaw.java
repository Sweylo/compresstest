/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package patterncompress;

import compresstest.Binary;
import static compresstest.Binary.*;

/**
 *
 * @author sweylo
 */
public class PatternRaw extends PatternDatum {
    
    private Binary data;
    int length;
    
    public PatternRaw(Binary data, int length) {
        
        if (length >= data.size()) {

            this.data = data;
            this.length = length;

            patternID = data.get(0) == ONE ? ONE_RAW_ID : ZERO_RAW_ID;
        
        } else {
            System.err.println("Length is shorter than data.");
            System.exit(1);
        }
        
    }
    
    @Override
	public String toString() {
		
        String pad = patternID == ZERO_RAW_ID ? "1" : "0";
        StringBuilder padBuilder = new StringBuilder();
		StringBuilder binBuilder = new StringBuilder();
        
        binBuilder.append(getBinaryID());
        binBuilder.append(" ");
        
        for (int i = 0; i < length - data.size(); i++) {
            padBuilder.append(pad);
        }
        
        binBuilder.append(padBuilder.toString());
        binBuilder.append(" ");
        binBuilder.append(data.toString());
		
		return binBuilder.toString();
		
	}
    
}
